import React from 'react'
import './style.css'

import Hobbies from '../../Components/Hobbies/index'
import Cards from '../../Components/Cards/index'
import Experience from '../../Components/Experience/index'

import ghilman from './img/ghilman anjum.png'
import linkedin from './img/linkedin.png'
import twitter from './img/twitter3.png'
import instagram from './img/Instagram.webp'
import snap from './img/react.png'
import html from './img/html2.png'
import java from './img/java2.png'
import aws from './img/aws2.png'
import kafka from './img/kafka.png'
import microsoft from './img/microsoft2.png'
import misa from './img/misa2.png'
import mongo from './img/mongo2.png'
import sql from './img/sql2.png'
import scss from './img/scss2.png'
import spring from './img/spring2.png'
import typescript from './img/typescript2.png'
import travel from './img/Travel.webp'
import book from './img/book-reading.jpg'
import snocker from './img/snocker.jpg'
import tennis from './img/table-tennis.jpg'
import blank from './img/twitter blank.png'
import iblank from './img/insta black.png'

const Portfolio = () => {
    return (
        <div>
            {/* ///////////    INTRO    //////////// */}
            <section className='intro'>
                <div className='img'>   <img src={ghilman} className='ghilman' /></div>
                <div className='name'>Ghilman Anjum</div>
                <div className='degree'>Senior Software Engineer</div>
                <div className='icons'>
                    <a href='https://www.linkedin.com/in/ghilman-anjum-448509136/'>
                        <img src={linkedin} className='linkedin' />
                    </a>
                    <a href='https://twitter.com/i/flow/login'>
                        <img src={twitter} className='twitter' />
                    </a>
                    <a href='https://www.instagram.com/g_h_i_l_m_a_n/'>
                        <img src={instagram} className='insta' />
                    </a>
                </div>
            </section>
            {/* ///////////////   BODY    /////////////////// */}
            <div className='container'>
                <section className='content-container'>
                    <div className='description'>
                        Faisal is a Co-Founder of Zayn Capital; He is the Managing Partner of Zayn BitRate Fund, a venture capital fund with a primary focus on seed stage ventures in Pakistan.
                    </div>
                    <div className='description'>
                        Faisal is also the Founder of BitRate Digital Assets, a Crypto Fund designed to give exposure to Institutional Investors & Family Offices.
                    </div>
                    <div className='description'>
                        His notable investment calls in Pakistan include some of the top names in the regional startup ecosystem: GrocerApp, Bazaar-tech, Laam.pk, NayaPay, Tazah, KTrade, Truck It In, Savyour, Krave Mart, Bookme.pk, PostEX, Roomy.pk, Scribe Audio, Bagallery.com, Trellis Housing Finance, MandiExpress.pk, Zaraye and KnowledgePlatform.com.
                    </div>
                    <div className='description'>
                        Previously, Faisal was a founding team member of Lakson Venture Capital (Lakson VC); he built the fund from the ground up and led the team as Managing Partner & Executive Director.
                    </div>
                    <div className='description'>
                        As an early Bitcoin and Ethereum investor, Faisal has a deep understanding of global macros and blockchain's implications on the digital economy.
                    </div>
                    <div className='description'>
                        Faisal has an MBA from Oxford University, joint Bachelor's and Master's degrees from Michigan State University USA. He has also been an investor in Hedge Funds, Venture Capital Funds and an active Angel Investor for over 15 years. Faisal is a limited partner of the Silicon Valley-based Venture Capital Fund 500 Startups.
                    </div>
                    <div className='separater'> </div>

                    {/* ///////////////   SECTION       ///////////////// */}
                    <section>
                        <div class="title">
                            My Services
                        </div>
                        <Cards />
                    </section>
                    <div className='separater'> </div>

                    {/* /////////////////    ARTICLES    /////////////// */}
                    <section>
                        <div className='title'>Conferences & Articles</div>
                        <div className='articleSection'>
                            <Hobbies image={travel} text='Travelling' />
                            <Hobbies image={book} text='Book Reading' />
                        </div>
                        <div className='articleSection '>
                            <Hobbies image={snocker} text='Snocker' />
                            <Hobbies image={tennis} text='Table Tennis' />
                        </div>

                    </section>

                    <div className='separater'> </div>

                    {/* ////////////   EDUCATION   /////////// */}
                    <section className='education'>
                        <div className='title'> Education </div>
                        <div className='school-education  '>
                            <div className='school'>
                                <div className='minhaj'><b>Minhaj Model School</b></div>
                                <div className='matriculation'>Matriculation Science </div>
                                <div className='year'>2009 - 2011</div>
                            </div>
                            <div className='school  '>
                                <div className='minhaj'><b>Superior College</b></div>
                                <div className='matriculation'>Intermediate Computer Science </div>
                                <div className='year'>2011 - 2013</div>
                            </div>
                            <div className='school'>
                                <div className='minhaj'><b>University of the Punjab</b></div>
                                <div className='matriculation'>Software Engineer, CSE </div>
                                <div className='year'>2013 - 2017</div>
                            </div>
                            <div className='school'>
                                <div className='minhaj'><b>University of the Punjab</b></div>
                                <div className='matriculation'>Software Engineer,CSE </div>
                                <div className='year'>2013 - 2017</div>
                            </div>
                            <div className='school'>
                                <div className='minhaj'><b>PUCIT</b></div>
                                <div className='matriculation'> Bachelor of Science - BS,  CSE </div>
                                <div className='year'>2013 - 2017</div>
                            </div>
                        </div>
                    </section>
                </section>
            </div>
            {/* ///////////   FOOTER    //////////////// */}
            <section className='footer'>
                <div className='connect'>Want to connect? Reach out below.</div>
                <div className='images'>
                    <a href='https://www.linkedin.com/in/ghilman-anjum-448509136/'>
                        <div className='linked'><b>in</b></div>
                    </a>
                    <a href='https://twitter.com/i/flow/login'>
                        <img src={blank} className='blank' />
                    </a>
                    <a href='https://www.instagram.com/g_h_i_l_m_a_n/'>
                        <img src={iblank} className='black' />
                    </a>

                </div>
                <div className='copyright'>Copyright @ 2022. All rights reserved</div>
            </section>

        </div >

    );
}
export default Portfolio;