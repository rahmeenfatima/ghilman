import PortfolioContainer from "./Containers/PortfolioContainer";


function App() {
  return (
    <div className="App">
        <PortfolioContainer/>
    </div>
  );
}

export default App;