import React from 'react'
import './style.css'

import snap from './img/react10.png'
import html from './img/html2.png'
import java from './img/java2.png'
import aws from './img/aws2.png'
import kafka from './img/kafka.png'
import microsoft from './img/microsoft2.png'
import misa from './img/misa2.png'
import mongo from './img/mongo2.png'
import sql from './img/sql11.png'
import scss from './img/scss2.png'
import spring from './img/spring2.png'
import typescript from './img/typescript2.png'
import travel from './img/Travel.webp'
import book from './img/book-reading.jpg'
import snocker from './img/snocker.jpg'
import tennis from './img/table-tennis.jpg'
import blank from './img/twitter blank.png'
import iblank from './img/insta black.png'

const Cards = ({ image, width, heading, content }) => {
    return (
        <div className='techStack'>
            <div className='row'>
                <div className='icon-name col-lg-3 col-md-6'>
                    <img src={snap} className='react' />
                    <div className='java'><a href='https://reactjs.org/docs/getting-started.html'> react</a></div>
                </div>
                <div className='icon-name col-lg-3 col-md-6'>
                    <img src={java}  className='react' />
                    <div className='java'>java</div>
                </div>
                <div className='icon-name  col-lg-3 col-md-6'>
                    <img src={html}style={{height:"60px"}} className='react' />
                    <div className='java'>html</div>
                </div>
                <div className='icon-name    col-lg-3 col-md-6'>
                    <img src={aws} className='react' />
                    <div className='java'>AWS</div>
                </div>
            </div>

            <div className='row'>
                <div className='icon-name  col-lg-3 col-md-6'>
                    <img src={kafka} style={{height:"70px"}} className='react' />
                    <div className='java'> Kafka</div>
                </div>
                <div className='icon-name   col-lg-3 col-md-6'>
                    <img src={microsoft} style={{height:"60px"}} className='react' />
                    <div className='java'>Microsoft Azure</div>
                </div>

                <div className='icon-name  col-lg-3 col-md-6'>
                    <img src={misa} className='react' />
                    <div className='java'>Misa</div>
                </div>
                <div className='icon-name  col-lg-3 col-md-6'>
                    <img src={mongo} style={{height:"90px"}} className='react' />
                    <div className='java'>Mongo</div>
                </div>
            </div>

            <div className='row'>
                <div className='icon-name   col-lg-3 col-md-6'>
                    <img src={sql} style={{height:"60px"}} className='react' />
                    <div className='java'> MySQL</div>
                </div>

                <div className='icon-name   col-lg-3 col-md-6'>
                    <img src={scss} style={{height:"60px"}} className='react' />
                    <div className='java'>SCSS </div>
                </div>
                <div className='icon-name  col-lg-3 col-md-6'>
                    <img src={spring} className='react' />
                    <div className='java'>Spring Boot</div>
                </div>
                <div className='icon-name   col-lg-3 col-md-6'>
                    <img src={typescript} style={{height:"50px"}} className='react' />
                    <div className='java'> Type Script</div>
                </div>
            </div>
        </div>
    );
}
export default Cards;