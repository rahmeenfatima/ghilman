import React from 'react'
import './style.css' 

const Hobbies = ({ image, text}) => {
    return (
        <div>
            <div className='hobby'>
                <img src={image} className='image5'/>
                <div className='text'>{text}</div>

            </div>
             

        </div>

    );
}
export default Hobbies;